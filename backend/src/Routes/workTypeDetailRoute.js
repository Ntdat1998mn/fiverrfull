const express = require("express");
const {
  getWorkDetailList,
  createWorkTypeDetail,
  getWorkTypeDetailById,
  editWorkTypeDetail,
  deleteWorkTypeDetail,
  createGroupWorkTypeDetail,
  editGroupWorkTypeDetail,
  workTypeDetailSearchPagination,
} = require("../controller/workTypeDetailController");
const { verifyToken } = require("../utils/jwt");

const workTypeDetailRoute = express.Router();

workTypeDetailRoute.get("", getWorkDetailList);
workTypeDetailRoute.post("", verifyToken, createWorkTypeDetail);
workTypeDetailRoute.get(
  "/work-type-detail-search-pagination",
  workTypeDetailSearchPagination
);
workTypeDetailRoute.get("/:id", getWorkTypeDetailById);
workTypeDetailRoute.put("/:id", verifyToken, editWorkTypeDetail);
workTypeDetailRoute.delete("/:id", verifyToken, deleteWorkTypeDetail);
workTypeDetailRoute.post(
  "/type-detail-group",
  verifyToken,
  createGroupWorkTypeDetail
);
workTypeDetailRoute.put(
  "/type-detail-group/:id",
  verifyToken,
  editGroupWorkTypeDetail
);

module.exports = workTypeDetailRoute;
