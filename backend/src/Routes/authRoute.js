const express = require("express");
const { signUp, signIn } = require("../controller/authController");

const authRoute = express.Router();

authRoute.post("/signUp", signUp);
authRoute.post("/signIn", signIn);

module.exports = authRoute;
