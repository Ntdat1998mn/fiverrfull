const { PrismaClient } = require("@prisma/client");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Lấy danh sách loại công việc
const getWorkTypeList = async (req, res) => {
  try {
    let data = await prisma.LoaiCongViec.findMany();
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách loại công việc thành công!");
    } else {
      failCode(res, data, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Thêm loại công việc
const createWorkType = async (req, res) => {
  try {
    let { id, tenLoaiCongViec } = req.body;
    let model = { id, tenLoaiCongViec };
    await prisma.LoaiCongViec.create({ data: model });
    successCode(res, model, "Thêm loại công việc thành công!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Phân trang tìm kiếm loại công việc
const workTypeSearchPagination = async (req, res) => {
  try {
    let { pageIndex, pageSize, keyWork } = req.body;
    let data = await prisma.$queryRaw`select * from (select * from LoaiCongViec 
        limit ${pageSize} offset ${(pageIndex - 1) * pageSize}) as Page
        where tenLoaiCongViec like ${`%${keyWork}%`};`;
    if (data.length > 0) {
      successCode(res, data, "Lấy loại công việc thành công!");
    } else {
      failCode(res, "Loại công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy công việc theo id loại công việc
const getWorkTypeById = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.LoaiCongViec.findFirst({ where: { id } });
    if (data) {
      successCode(res, data, "Lấy loại công việc thành công!");
    } else {
      failCode(res, "Loại công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Sửa công việc theo id loại công việc
const editWorkType = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let { tenLoaiCongViec } = req.body;
    let model = { tenLoaiCongViec };
    await prisma.LoaiCongViec.update({
      data: model,
      where: { id },
    });
    successCode(res, model, "Chỉnh sửa loại công việc thành công!");
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Loại công việc không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Xoá công việc theo id loại công việc
const deleteWorkType = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.LoaiCongViec.delete({ where: { id } });
    successCode(res, data, "Xoá loại công việc thành công!");
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Loại công việc không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

module.exports = {
  getWorkTypeList,
  createWorkType,
  getWorkTypeById,
  editWorkType,
  deleteWorkType,
  workTypeSearchPagination,
};
