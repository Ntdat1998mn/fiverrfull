const express = require("express");
const {
  getCommentList,
  createComment,
  editComment,
  deleteComment,
  getCommentById,
  getCommentByWorkId,
} = require("../controller/commentController");
const { verifyToken } = require("../utils/jwt");

const commentRoute = express.Router();

commentRoute.get("", getCommentList);
commentRoute.post("", verifyToken, createComment);
commentRoute.put("/:id", verifyToken, editComment);
commentRoute.delete("/:id", verifyToken, deleteComment);
commentRoute.get("/work/:id", getCommentByWorkId);
commentRoute.get("/:id", getCommentById);

module.exports = commentRoute;
