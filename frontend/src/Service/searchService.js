import axios from "axios";
import { BASE_URL } from "./configURL";

export const searchService = {
  getCongViecTheoTen: (name) => {
    return axios({
      url: BASE_URL + `/work/work-list/${name}`,
      method: "GET",
    });
  },
};
