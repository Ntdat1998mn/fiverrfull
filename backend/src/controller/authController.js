const { PrismaClient } = require("@prisma/client");
const bcrypt = require("bcryptjs");
const { creatToken } = require("../utils/jwt");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

//Đăng ký
const signUp = async (req, res) => {
  try {
    let {
      name,
      email,
      password,
      age,
      phone,
      birthday,
      gender,
      skill,
      certification,
    } = req.body;
    let model = {
      name,
      email,
      password: bcrypt.hashSync(password, 10),
      age,
      phone,
      birthday,
      gender,
      skill,
      certification,
    };
    let checkEmail = await prisma.NguoiDung.findFirst({ where: { email } });
    if (checkEmail) {
      model.password = "";
      failCode(res, "Email đã tồn tại!");
    } else {
      let data = await prisma.NguoiDung.create({ data: model });
      console.log("data: ", data);
      data.password = "";
      successCode(res, data, "Đăng ký thành công!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Đăng nhập
const signIn = async (req, res) => {
  try {
    let { email, password } = req.body;
    let data = await prisma.NguoiDung.findFirst({ where: { email } });
    if (data) {
      let checkPassword = bcrypt.compareSync(password, data.password);
      if (checkPassword) {
        let token = creatToken(data);
        /* res.cookie("token", token, {
          httpOnly: true,
          maxAge: 86400000,
        }); */
        data.password = "";
        successCode(res, { ...data, token }, "Đăng nhập thành công!");
      } else {
        password = "";
        failCode(res, "Mật khẩu không đúng!");
      }
    } else {
      failCode(res, "Email không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = { signIn, signUp };
