import React from "react";
import { NavLink } from "react-router-dom";

export default function GroupWorkListItem({ workType }) {
  const renderTypeDetail = (data) => {
    return data?.map((item, index) => {
      return (
        <NavLink key={index} to={`/categories/${item.id}`}>
          <p> {item.tenChiTiet}</p>
        </NavLink>
      );
    });
  };
  const renderGroupWorkItem = () => {
    return workType.NhomChiTietLoai?.map((workTypeGroup, index) => {
      return (
        <div key={index} className="item">
          <img src={workTypeGroup.hinhAnh} alt="..." />
          <h1 className="font-semibold text-xl mt-4">
            {workTypeGroup.tenNhomChiTietLoaiCongViec}
          </h1>
          <div className="font-normal text-lg text-gray-500 mt-4">
            {renderTypeDetail(workTypeGroup.ChiTietLoaiCongViec)}
          </div>
        </div>
      );
    });
  };
  return (
    <>
      <h1 className="text-gray-700 text-2xl font-semibold mb-3">
        Explore {workType.tenLoaiCongViec}
      </h1>
      <div className="grid gap-3 grid-cols-1 min-[600px]:grid-cols-2 min-[992px]:grid-cols-4">
        {renderGroupWorkItem()}
      </div>
    </>
  );
}
