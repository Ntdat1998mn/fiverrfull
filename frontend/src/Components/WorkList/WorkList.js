import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { searchService } from "../../Service/searchService";
import { workListService } from "../../Service/workListService";
import WorkItem from "./WorkItem";

export default function WorkList() {
  let { id, name } = useParams();

  const [workList, setWorkList] = useState([]);
  useEffect(() => {
    if (id) {
      workListService
        .getWork(id)
        .then((res) => {
          setWorkList(res.data.data.CongViec);
        })
        .catch((err) => {
          console.log(err);
        });
    } else if (name) {
      searchService
        .getCongViecTheoTen(name)
        .then((res) => {
          setWorkList(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [workList]);
  let renderWorkList = () => {
    return workList.map((work, index) => {
      return (
        <div key={index} className="mb-4 border border-gray-300">
          <WorkItem work={work} />
        </div>
      );
    });
  };

  return (
    <div className="container mx-auto mt-48 mb-4">
      <div className="flex justify-between mb-4 font-medium text-gray-500">
        <div className="">{workList.length} services available</div>
        <div>
          <span>Sort by</span>
          <select className="font-bold text-black" name="sort" id="sort">
            <option value="1">Relevance</option>
            <option value="2">Best Selling</option>
            <option value="3">New Arrivals</option>
          </select>
        </div>
      </div>
      <div className="grid gap-5 grid-cols-1 min-[600px]:grid-cols-2 min-[992px]:grid-cols-4 ">
        {renderWorkList()}
      </div>
    </div>
  );
}
