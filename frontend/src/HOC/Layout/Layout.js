import React from "react";
import Footer from "../../Components/Footer/Footer";
import HeaderNavBar from "../../Components/Header/HeaderNavBar/HeaderNavBar";
import MobileNavBar from "../../Components/Header/HeaderNavBar/MobileNavBar";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";

export default function Layout({ Component }) {
  return (
    <div>
      <Desktop>
        <HeaderNavBar />
        <div className="mt-20">
          <Component />
        </div>
        <Footer />
      </Desktop>
      <Mobile>
        <MobileNavBar />
        <div className="mt-20">
          <Component />
        </div>
        <Footer />
      </Mobile>
      <Tablet>
        <MobileNavBar />
        <div className="mt-20">
          <Component />
        </div>
        <Footer />
      </Tablet>
    </div>
  );
}
