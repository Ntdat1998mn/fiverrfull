import React from 'react'
import DetailBookedJob from '../../Components/Profile/DetailBookedJob'
import DetailProfile from '../../Components/Profile/DetailProfile'
import "./Profile.css"
import {Desktop, Mobile, Tablet} from "../../HOC/Responsive/Responsive"
import MobileDetailProfile from '../../Components/Profile/MobileDetailProfile'

export default function Profile() {
  return (
    <div id='profile'>
      <Desktop>
        <div className='mb-5 mt-32 container mx-auto'>
          <div className='grid grid-cols-3'>
            <div>
              <DetailProfile></DetailProfile>
            </div>
            <div className='col-span-2'>
              <DetailBookedJob></DetailBookedJob>
            </div>
          </div>
        </div>
      </Desktop>
      <Mobile>
        <div className='mb-5 mt-32 container mx-auto'>
          <div className='grid grid-cols-1'>
            <div>
              <MobileDetailProfile></MobileDetailProfile>
            </div>
            <div className='col-span-2'>
              <DetailBookedJob></DetailBookedJob>
            </div>
          </div>
        </div>
      </Mobile>
      <Tablet>
        <div className='mb-5 mt-32 container mx-auto'>
          <div className='grid grid-cols-1'>
            <div>
              <MobileDetailProfile></MobileDetailProfile>
            </div>
            <div className='col-span-2'>
              <DetailBookedJob></DetailBookedJob>
            </div>
          </div>
        </div>
      </Tablet>
    </div>
  )
}
