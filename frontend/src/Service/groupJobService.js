import axios from "axios";
import { BASE_URL } from "./configURL";

export const groupJobService = {
  getLoaiCongViec: () => {
    return axios({
      url: BASE_URL + "/work",
      method: "GET",
    });
  },
  getMenuCongViec: () => {
    return axios({
      url: BASE_URL + "/work/work-type-menu",
      method: "GET",
    });
  },
  xoaCongViecDaThue: (id, token) => {
    return axios({
      url: BASE_URL + `/hire-work/${id}`,
      method: "DELETE",
      headers: { accessToken: token },
    });
  },
};
