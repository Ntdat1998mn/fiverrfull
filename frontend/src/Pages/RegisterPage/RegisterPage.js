import React from "react";
import signup from "../../assets/signup.jpg";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";
import {
  regexDate,
  regexEmail,
  regexPassword,
  regexPhoneNumber,
} from "./validate";
import { message } from "antd";
import { userService } from "../../Service/userService";

export default function RegisterPage() {
  let navigate = useNavigate();
  let [valid, setValid] = useState(false);
  let [errors, setErrors] = useState({});
  let [userForm, setUserForm] = useState({
    name: "",
    email: "",
    password: "",
    rePassword: "",
    phone: "",
    birthday: "",
    gender: "unknow",
    agree: false,
  });
  let isValid = () => {
    for (let name in userForm) {
      let value = userForm[name];
      if (value === "") {
        errors = { ...errors, [`${name}Err`]: "Can not be emtpy!" };
      } else if (name === "email" && !regexEmail.test(value)) {
        errors = { ...errors, emailErr: "Please Input abc@xyz.com" };
      } else if (name === "password" && !regexPassword.test(value)) {
        errors = {
          ...errors,
          passwordErr:
            "Atleast 8 characters, 1 digit, 1 Uppercase, 1 lowercase",
        };
      } else if (name === "rePassword" && value !== userForm.password) {
        errors = { ...errors, rePasswordErr: "Not same password" };
      } else if (name == "phone" && !regexPhoneNumber.test(value)) {
        errors = { ...errors, phoneErr: "Only Vietnamese phone number" };
      } else if (name === "birthday" && !regexDate.test(value)) {
        errors = { ...errors, birthdayErr: "yyyy-mm-dd" };
      } else if (name === "agree" && value === false) {
        errors = { ...errors, agreeErr: "You need to agree all statement!" };
      }
    }
    if (Object.keys(errors).length > 0) {
      return false;
    } else return true;
  };
  const handleChangeForm = (e) => {
    const { name, value } = e.target;
    setUserForm({ ...userForm, [name]: value });
    if (errors.emailErr === "Email đã tồn tại!") {
      delete errors.emailErr;
    }
  };
  const handleAgree = (e) => {
    const { name, checked } = e.target;
    setUserForm({ ...userForm, [name]: checked });
  };
  if (valid) {
    isValid();
  }
  const handleSubmitForm = (e) => {
    e.preventDefault();
    setValid(true);
    if (!isValid()) {
      return false;
    } else {
      userService
        .getSignUp(userForm)
        .then(() => {
          message.success("Đăng ký thành công!");
          setTimeout(() => {
            navigate("/login");
          }, 1000);
        })
        .catch((err) => {
          if (err.response.data.message === "Email đã tồn tại!") {
            setErrors({ ...errors, emailErr: "Email đã tồn tại!" });
          }
        });
    }
  };

  const renderRegister = () => {
    return (
      <div>
        <h2 className="text-center text-green-500 font-semibold text-3xl pb-5">
          REGISTER
        </h2>
        <form className="text-base">
          <div className="flex">
            <label className="w-10" htmlFor="name">
              <i className="fa fa-user mr-2"></i>:
            </label>
            <div className="w-full">
              <input
                className="w-full text-gray-500 border border-gray-500 px-2 py-1 rounded"
                placeholder="Your Name"
                value={userForm.name}
                name="name"
                onChange={handleChangeForm}
              />
              <span className="text-red-500">{errors.nameErr}</span>
            </div>
          </div>
          <div className="flex mt-4">
            <label className="w-10" htmlFor="email">
              <i className="fa fa-envelope mr-2"></i>:
            </label>
            <div className="w-full">
              <input
                className="w-full text-gray-500 border border-gray-500 px-2 py-1 rounded"
                value={userForm.email}
                onChange={handleChangeForm}
                placeholder="Your Email"
                name="email"
              />
              <span className="text-red-500">{errors.emailErr}</span>
            </div>
          </div>
          <div className="flex mt-4">
            <label className="w-10" htmlFor="password">
              <i className="fa fa-lock mr-2"></i>:
            </label>
            <div className="w-full">
              <input
                type="password"
                className="w-full text-gray-500 border border-gray-500 px-2 py-1 rounded"
                value={userForm.password}
                onChange={handleChangeForm}
                placeholder="Your Password"
                name="password"
              />
              <span className="text-red-500">{errors.passwordErr}</span>
            </div>
          </div>
          <div className="flex mt-4">
            <label className="w-10" htmlFor="rePassword">
              <i className="fa fa-key mr-2"></i>:
            </label>
            <div className="w-full">
              <input
                type="password"
                className="w-full text-gray-500 border border-gray-500 px-2 py-1 rounded"
                value={userForm.rePassword}
                onChange={handleChangeForm}
                placeholder="Repeat Your Password"
                name="rePassword"
              />
              <span className="text-red-500">{errors.rePasswordErr}</span>
            </div>
          </div>
          <div className="flex mt-4">
            <label className="w-10" htmlFor="phone">
              <i className="fa fa-phone mr-2"></i>:
            </label>
            <div className="w-full">
              <input
                className="w-full text-gray-500 border border-gray-500 px-2 py-1 rounded"
                value={userForm.phone}
                onChange={handleChangeForm}
                placeholder="Your Phone"
                name="phone"
              />
              <span className="text-red-500">{errors.phoneErr}</span>
            </div>
          </div>
          <div className="flex mt-4">
            <label className="w-10" htmlFor="birthday">
              <i className="fa fa-birthday-cake mr-2"></i>:
            </label>
            <div className="w-full">
              <input
                type="date"
                className="w-full text-gray-500 border border-gray-500 px-2 py-1 rounded"
                value={userForm.birthday}
                onChange={handleChangeForm}
                pattern="\d{4}-\d{2}-\d{2}"
                name="birthday"
              />
              <span className="text-red-500">{errors.birthdayErr}</span>
            </div>
          </div>
          <div className="flex mt-4">
            <label className="w-10">
              <i className="fa fa-venus-mars mr-2"></i>:
            </label>
            <div className="w-full flex">
              <div className="block">
                <input
                  type="radio"
                  className="text-gray-500 border border-gray-500 px-2 py-1 rounded mr-2"
                  value="male"
                  name="gender"
                />
                <label htmlFor="male" className="mr-6">
                  Male
                </label>
              </div>
              <div className="block">
                <input
                  type="radio"
                  className="text-gray-500 border border-gray-500 px-2 py-1 mr-2 rounded"
                  value="female"
                  name="gender"
                />
                <label htmlFor="male">Female</label>
              </div>
            </div>
          </div>
          <div className="flex mt-4 ml-9">
            <div className="block">
              <input
                type="checkbox"
                className="text-gray-500 border border-gray-500 px-2 py-1 mr-2 rounded"
                name="agree"
                onChange={handleAgree}
              />
              <label htmlFor="male">
                I agree all statements in{" "}
                <a className="hover:text-blue-400 text-gray-400" href="#">
                  Terms of service
                </a>
              </label>{" "}
              <p className="text-red-500">{errors.agreeErr}</p>
            </div>
          </div>

          {/* <Form.Item label={<i className="fa fa-venus-mars mr-2"></i>}>
            <Radio.Group
              name="gender"
              value={userForm.gender}
              onChange={handleChangeForm}
            >
              <Radio value="male"> Male </Radio>
              <Radio value={false}> Female </Radio>
            </Radio.Group>
            <p className="text-red-500" id="gender"></p>
          </Form.Item> */}
        </form>
        <button
          onClick={handleSubmitForm}
          className="mt-5 px-5 py-2 bg-green-500 text-white font-semibold text-xl rounded-3xl"
        >
          Submit
        </button>
        <a className="ml-5 text-blue-500 underline" href="/login">
          Already have an account?
        </a>
      </div>
    );
  };
  return (
    <div id="register">
      <Desktop>
        <div className="mb-10 mt-32 p-5 grid grid-cols-2 w-3/5 mx-auto shadow-lg rounded-xl">
          <div className="left">{renderRegister()}</div>
          <div className="right mx-auto self-center">
            <img src={signup} alt="" />
          </div>
        </div>
      </Desktop>
      <Mobile>
        <div className="mb-10 mt-32 p-5 grid grid-cols-1 w-full mx-auto shadow-lg rounded-xl">
          <div className="left">{renderRegister()}</div>
        </div>
      </Mobile>
      <Tablet>
        <div className="mb-10 mt-32 p-5 grid grid-cols-2 w-11/12 mx-auto shadow-lg rounded-xl">
          <div className="left">{renderRegister()}</div>
          <div className="right mx-auto self-center">
            <img src={signup} alt="" />
          </div>
        </div>
      </Tablet>
    </div>
  );
}
