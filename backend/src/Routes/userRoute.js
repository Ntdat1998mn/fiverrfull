const express = require("express");
const {
  getUserList,
  createUser,
  deleteUser,
  getUserById,
  editUser,
  getUserListByName,
  userSearchPagination,
} = require("../controller/userController");
const { verifyToken } = require("../utils/jwt");
const userRoute = express.Router();

userRoute.get("", getUserList);
userRoute.post("", verifyToken, verifyToken, createUser);
userRoute.delete("", verifyToken, verifyToken, deleteUser);
userRoute.get("/user-search-pagination", userSearchPagination);
userRoute.get("/:id", getUserById);
userRoute.put("/:id", verifyToken, editUser);
userRoute.get("/search/:name", getUserListByName);

module.exports = userRoute;
