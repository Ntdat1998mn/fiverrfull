import axios from "axios";
import { BASE_URL } from "./configURL";

export const userService = {
  logIn: (model) => {
    return axios({
      url: BASE_URL + "/auth/signIn",
      method: "POST",
      data: model,
    });
  },
  getSignUp: (data) => {
    return axios({
      url: BASE_URL + "/auth/signUp",
      method: "POST",
      data: data,
    });
  },
  getUpdate: (data, id, token) => {
    return axios({
      url: BASE_URL + "/user/" + id,
      method: "PUT",
      data: data,
      headers: { accessToken: token },
    });
  },
  getUser: (id) => {
    return axios({
      url: BASE_URL + "/user/" + id,
      method: "GET",
    });
  },
  uploadAvatar: (data, token) => {
    return axios({
      url: BASE_URL + "/users/upload-avatar",
      method: "POST",
      data: data,
      headers: { accessToken: token },
    });
  },
  getThueCongViec: (id, token) => {
    return axios({
      url: BASE_URL + `/hire-work/hire-work-list-by-user-id/${id}`,
      method: "GET",
      headers: { accessToken: token },
    });
  },
};
