import React, { useState } from "react";
import { Form, Input, message, Modal, Radio } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { setOpen, setUser } from "../../Redux-toolkit/slice/userSlice";
import { userService } from "../../Service/userService";
import { userLocalService } from "../../Service/localService";
import { regexDate, regexPhoneNumber } from "../../Pages/RegisterPage/validate";
import { useNavigate } from "react-router-dom";

export default function ModalUpdate() {
  let dispatch = useDispatch();
  const navigate = useNavigate();
  let user = useSelector((state) => state.userSlice.user);
  let open = useSelector((state) => state.userSlice.open);
  let [cloneUser, setCloneUser] = useState({ ...user });
  let userForm = {
    name: "",
    phone: "",
    birthday: "",
    skill: "",
    certification: "",
  };
  let errors = {};
  let [valid, setValid] = useState(false);
  const handleOk = () => {};

  const handleCancel = () => {
    dispatch(setOpen(false));
  };

  let isValid = () => {
    for (let name in userForm) {
      let value = cloneUser[name];
      if ((value === "") | (value === null)) {
        errors = { ...errors, [`${name}Err`]: "Can not be emtpy!" };
      } else if (name == "phone" && !regexPhoneNumber.test(value)) {
        errors = { ...errors, phoneErr: "Only Vietnamese phone number" };
      } else if (name === "birthday" && !regexDate.test(value)) {
        errors = { ...errors, birthdayErr: "yyyy-mm-dd" };
      }
    }
    if (Object.keys(errors).length > 0) {
      return false;
    } else return true;
  };

  if (valid) {
    isValid();
  }
  const handleChangeForm = (e) => {
    let { name, value } = e.target;
    setCloneUser({ ...cloneUser, [name]: value });
  };
  const handleSubmitForm = (e) => {
    e.preventDefault();
    setValid(true);
    if (isValid()) {
      userService
        .getUpdate(cloneUser, cloneUser.id, cloneUser.token)
        .then(() => {
          message.success("Chỉnh sửa thông tin cá nhân thành công!");
          userLocalService.setUser(cloneUser);
          setTimeout(() => {
            window.location.reload();
          }, 1000);
        })
        .catch(() => {
          message.error("Đã có lỗi!");
        });
    } else return true;
  };
  const renderForm = () => {
    return (
      <div>
        <div className="grid grid-cols-2 mx-3 gap-2">
          <Form.Item label="Email">
            <Input value={user.email} disabled />
          </Form.Item>
          <Form.Item label="Phone">
            <Input
              value={cloneUser.phone}
              name="phone"
              onChange={handleChangeForm}
            />
            <span className="text-red-500">{errors.phoneErr}</span>
          </Form.Item>
          <Form.Item label="Name">
            <Input
              name="name"
              onChange={handleChangeForm}
              value={cloneUser.name}
            />
            <span className="text-red-500">{errors.nameErr}</span>
          </Form.Item>
          <Form.Item label="Birthday">
            <Input
              name="birthday"
              onChange={handleChangeForm}
              value={cloneUser.birthday}
            />
            <span className="text-red-500">{errors.birthdayErr}</span>
          </Form.Item>
        </div>
        <div className="mx-3">
          <Form.Item label="Gender">
            <Radio.Group
              onChange={handleChangeForm}
              name="gender"
              value={cloneUser.gender}
            >
              <Radio value="male"> Male </Radio>
              <Radio value="female"> Female </Radio>
            </Radio.Group>
          </Form.Item>
        </div>
        <div className="grid grid-cols-1 lg:grid-cols-2 mx-3 gap-2">
          <Form.Item style={{ width: "100%" }} label="Certification">
            <Input
              name="certification"
              onChange={handleChangeForm}
              value={cloneUser.certification}
              placeholder="Example: CyberSoft, Udemy,.."
            />
            <span className="text-red-500">{errors.certificationErr}</span>
          </Form.Item>
          <Form.Item style={{ width: "100%" }} label="Skills">
            <Input
              name="skill"
              onChange={handleChangeForm}
              value={cloneUser.skill}
              placeholder="Example: ReactJs, Javascript, Design,.."
            />
            <span className="text-red-500">{errors.skillErr}</span>
          </Form.Item>
        </div>
      </div>
    );
  };
  return (
    <div id="modalUpdate">
      <Modal
        open={open}
        title={<p className="text-center text-2xl">Update User</p>}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <button
            className="bg-red-500 text-white text-md font-semibold py-3 px-4 rounded-xl mr-3 hover:shadow-2xl"
            key="back"
            onClick={handleCancel}
          >
            Cancel
          </button>,
          <button
            className="bg-green-500 text-white text-md font-semibold py-3 px-4 rounded-xl mr-3 hover:shadow-2xl"
            type="primary"
            onClick={handleSubmitForm}
          >
            Save
          </button>,
        ]}
      >
        {renderForm()}
      </Modal>
    </div>
  );
}
