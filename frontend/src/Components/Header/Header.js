import React from "react";
import HeaderNavBar from "./HeaderNavBar/HeaderNavBar";
import "./Header.css";
import HeaderCarousel from "./Carousel/HeaderCarousel";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";
import MobileCarousel from "./Carousel/MobileCarousel";
import MobileNavBar from "./HeaderNavBar/MobileNavBar";

export default function Header() {
  return (
    <div id="header">
      <Desktop>
        <HeaderNavBar />
        <HeaderCarousel />
      </Desktop>
      <Mobile>
        <MobileNavBar />
        <MobileCarousel />
      </Mobile>
      <Tablet>
        <MobileNavBar />
        <MobileCarousel />
      </Tablet>
    </div>
  );
}
