import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import Layout from "./HOC/Layout/Layout";
import RegisterPage from "./Pages/RegisterPage/RegisterPage";
import Profile from "./Pages/Profile/Profile";
import WorkList from "./Components/WorkList/WorkList";
import GroupWorkList from "./Components/GroupWorkList/GroupWorkList";
import JobDetail from "./Components/JobDetail/JobDetail";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route
          path="/login"
          element={<Layout Component={LoginPage}></Layout>}
        />
        <Route
          path="/register"
          element={<Layout Component={RegisterPage}></Layout>}
        />
        <Route
          path="/profile"
          element={<Layout Component={Profile}></Layout>}
        />
        <Route
          path="/categories/:id"
          element={<Layout Component={WorkList}></Layout>}
        />
        <Route
          path="/title/:groupId"
          element={<Layout Component={GroupWorkList}></Layout>}
        />
        <Route
          path="/jobDetail/:id"
          element={<Layout Component={JobDetail}></Layout>}
        />
        <Route
          path="/result/:name"
          element={<Layout Component={WorkList}></Layout>}
        />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
