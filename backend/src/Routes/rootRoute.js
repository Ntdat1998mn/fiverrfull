const express = require("express");
const authRoute = require("./authRoute");
const commentRoute = require("./commentRoute");
const workTypeDetailRoute = require("./workTypeDetailRoute");
const workTypeRoute = require("./workTypeRoute");
const workRoute = require("./workRoute");
const userRoute = require("./userRoute");
const hireWorkRoute = require("./hireWorkRoute");
const rootRoute = express.Router();

rootRoute.use("/auth", authRoute);

rootRoute.use("/user", userRoute);

rootRoute.use("/comment", commentRoute);

rootRoute.use("/work", workRoute);

rootRoute.use("/work-type", workTypeRoute);

rootRoute.use("/work-type-detail", workTypeDetailRoute);

rootRoute.use("/hire-work", hireWorkRoute);

module.exports = rootRoute;
