export const regexEmail =
  /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const regexPassword =
  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
export const regexPhoneNumber = /(84|0[3|5|7|8|9])+([0-9]{8})\b/;
export const regexDate = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/;

/* export const validEmail = (value) => {
  let result = {
    valid: null,
    message: "",
  };
  const regexEmail =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  let isValid = regex.test(value);
  if (isValid) {
    return (result = {
      valid: true,
      message: "",
    });
  } else {
    return (result = {
      valid: false,
      message: "Please Input abc@xyz.com",
    });
  }
};
export const validPassword = (value) => {
  let result = {
    valid: null,
    message: "",
  };
  const regexPassword =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
  let isValid = regex.test(value);
  if (isValid) {
    return (result = {
      valid: true,
      message: "",
    });
  } else {
    return (result = {
      valid: false,
      message: "Atleast 8 characters, 1 digit, 1 Uppercase, 1 lowercase",
    });
  }
};
export const validNumber = (value) => {
  let result = {
    valid: null,
    message: "",
  };
  const regexNumber = /^\d+$/;
  let isValid = regex.test(value);
  if (isValid) {
    return (result = {
      valid: true,
      message: "",
    });
  } else {
    return (result = {
      valid: false,
      message: "Only Number",
    });
  }
};
export const validDate = (value) => {
  let result = {
    valid: null,
    message: "",
  };
  const regexDate = /^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/;
  let isValid = regex.test(value);
  if (isValid) {
    return (result = {
      valid: true,
      message: "",
    });
  } else {
    return (result = {
      valid: false,
      message: "yyyy-mm-dd",
    });
  }
};
 */
