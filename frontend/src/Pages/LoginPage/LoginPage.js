import React, { useState } from "react";
import { Form, Input, message } from "antd";
import signIn from "../../assets/signIn.jpg";
import { userService } from "../../Service/userService";
import { useDispatch, useSelector } from "react-redux";
import { setUser } from "../../Redux-toolkit/slice/userSlice";
import { userLocalService } from "../../Service/localService";
import { useNavigate } from "react-router-dom";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";

export default function LoginPage() {
  const dispatch = useDispatch();
  let user = useSelector((state) => state.userSlice?.user);
  const navigate = useNavigate();
  const [err, setErr] = useState("");
  const onFinish = (values) => {
    let model = {
      email: values.email,
      password: values.password,
    };
    userService
      .logIn(model)
      .then((res) => {
        dispatch(setUser(res.data.data));
        userLocalService.setUser(res.data.data);
        message.success("Login Success!");
      })
      .catch(() => {
        setErr("Email or Password is Invalid. Check Again!");
      });
  };
  const setNavigate = () => {
    setTimeout(() => {
      navigate("/");
    }, 1000);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const renderLogin = () => {
    if (user) {
      return (
        <>
          <h2 className="pb-10 text-center text-3xl font-bold text-green-500">
            Sign In to Fiverr
          </h2>
          <h4 className="text-red-500 font-bold text-3xl text-center">
            You Have Already Logged In!
          </h4>
          {setNavigate()}
        </>
      );
    } else {
      return (
        <>
          <h2 className="pb-10 text-center text-3xl font-bold text-green-500">
            Sign In to Fiverr
          </h2>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              label="Your Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            >
              <Input placeholder="Input Your Email" />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password placeholder="Input Your Password" />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <div>
                <p className="mb-3 text-red-500">{err}</p>
                <button className="bg-green-500 text-white py-2 px-5 text-lg font-semibold rounded-3xl hover:bg-green-700 hover:animate-bounce">
                  LOGIN
                </button>
                <a className="text-blue-500 ml-5 underline " href="/register">
                  Register Now?
                </a>
              </div>
            </Form.Item>
          </Form>
        </>
      );
    }
  };
  return (
    <div id="login">
      <Desktop>
        <div className="grid grid-cols-2 w-3/5 mx-auto mt-32 mb-20 p-5 shadow-md rounded-lg">
          <div>
            <img src={signIn} alt="" />
          </div>
          <div className="self-center">{renderLogin()}</div>
        </div>
      </Desktop>
      <Mobile>
        <div className="grid grid-cols-1 w-full mx-auto mt-32 mb-20 p-5 shadow-md rounded-lg overflow-hidden">
          <div className="self-center">{renderLogin()}</div>
        </div>
      </Mobile>
      <Tablet>
        <div className="grid grid-cols-1 w-full mx-auto mt-32 mb-20 p-5 shadow-md rounded-lg overflow-hidden">
          <div className="self-center">{renderLogin()}</div>
        </div>
      </Tablet>
    </div>
  );
}
