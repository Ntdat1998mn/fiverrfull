const USER_LOGIN = "USER_LOGIN";
export const userLocalService = {
    setUser: (userData) => {
        let userJSON = JSON.stringify(userData);
        localStorage.setItem(USER_LOGIN,userJSON);
    },
    getUser: () => {
        let userJSON = localStorage.getItem(USER_LOGIN);
        if (userJSON != null){
            return JSON.parse(userJSON);
        }else{
            return null;
        }
    },
    remove: () => {
        localStorage.removeItem(USER_LOGIN)
    }
}