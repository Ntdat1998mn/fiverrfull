const express = require("express");
const {
  getWorkTypeList,
  createWorkType,
  getWorkTypeById,
  editWorkType,
  deleteWorkType,
  workTypeSearchPagination,
} = require("../controller/workTypeController");
const { verifyToken } = require("../utils/jwt");

const workTypeRoute = express.Router();

workTypeRoute.get("", getWorkTypeList);
workTypeRoute.post("", verifyToken, createWorkType);
workTypeRoute.get("/work-type-search-pagination", workTypeSearchPagination);
workTypeRoute.get("/:id", getWorkTypeById);
workTypeRoute.put("/:id", verifyToken, editWorkType);
workTypeRoute.delete("/:id", verifyToken, deleteWorkType);

module.exports = workTypeRoute;
