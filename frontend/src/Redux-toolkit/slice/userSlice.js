import { createSlice } from '@reduxjs/toolkit'
import { userLocalService } from '../../Service/localService'

const initialState = {
  user: userLocalService?.getUser(),
  open: false,
}

export const userSlice = createSlice({
  name: 'userSlice',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = {...action.payload};
    },
    setOpen: (state,action) => {
      state.open = action.payload
    }
  },
})

// Action creators are generated for each case reducer function
export const { setUser,setOpen} = userSlice.actions

export default userSlice.reducer