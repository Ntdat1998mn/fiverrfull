-- Adminer 4.8.1 MySQL 8.0.31 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `BinhLuan`;
CREATE TABLE `BinhLuan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `maCongViec` int NOT NULL,
  `maNguoiBinhLuan` int NOT NULL,
  `ngayBinhLuan` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `noiDung` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saoBinhLuan` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maCongViec` (`maCongViec`),
  KEY `maNguoiBinhLuan` (`maNguoiBinhLuan`),
  CONSTRAINT `BinhLuan_ibfk_1` FOREIGN KEY (`maNguoiBinhLuan`) REFERENCES `NguoiDung` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BinhLuan_ibfk_2` FOREIGN KEY (`maCongViec`) REFERENCES `CongViec` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `ChiTietLoaiCongViec`;
CREATE TABLE `ChiTietLoaiCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tenChiTiet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinhAnh` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maNhomChiTietLoaiCongViec` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maNhomChiTietLoaiCongViec` (`maNhomChiTietLoaiCongViec`),
  CONSTRAINT `ChiTietLoaiCongViec_ibfk_2` FOREIGN KEY (`maNhomChiTietLoaiCongViec`) REFERENCES `NhomChiTietLoai` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `ChiTietLoaiCongViec` (`id`, `tenChiTiet`, `hinhAnh`, `maNhomChiTietLoaiCongViec`) VALUES
(1,	'Short Video Ads',	NULL,	1),
(2,	'Social Media Videos',	NULL,	1),
(3,	'Video Editing',	NULL,	2),
(4,	'Visual Effects',	NULL,	2),
(5,	'Producers & Composers',	NULL,	3),
(7,	'Songwriters',	NULL,	3),
(8,	'Articles & Blog Posts',	NULL,	4),
(9,	'Social Media Advertising',	NULL,	5),
(10,	'Video Editing',	NULL,	5),
(11,	'Visual Effects',	NULL,	5),
(12,	'Visual Effects',	NULL,	6),
(13,	'Songwriters',	NULL,	6),
(14,	'Video Editing',	NULL,	7),
(15,	'Producers & Composers',	NULL,	7),
(16,	'Social Media Advertising',	NULL,	8),
(17,	'Visual Effects',	NULL,	8),
(18,	'Social Media Videos',	NULL,	9),
(19,	'Video Editing',	NULL,	9);

DROP TABLE IF EXISTS `CongViec`;
CREATE TABLE `CongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tenCongViec` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `danhGia` int NOT NULL,
  `giaTien` int NOT NULL,
  `hinhAnh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `moTa` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `moTaNgan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saoCongViec` int NOT NULL,
  `maChiTietLoaiCongViec` int NOT NULL,
  `nguoiTao` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maChiTietLoaiCongViec` (`maChiTietLoaiCongViec`),
  KEY `nguoiTao` (`nguoiTao`),
  KEY `saoCongViec` (`saoCongViec`),
  CONSTRAINT `CongViec_ibfk_1` FOREIGN KEY (`nguoiTao`) REFERENCES `NguoiDung` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `CongViec_ibfk_3` FOREIGN KEY (`maChiTietLoaiCongViec`) REFERENCES `ChiTietLoaiCongViec` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `CongViec` (`id`, `tenCongViec`, `danhGia`, `giaTien`, `hinhAnh`, `moTa`, `moTaNgan`, `saoCongViec`, `maChiTietLoaiCongViec`, `nguoiTao`) VALUES
(1,	'I will design minimal logo with complete corporate brand identity',	66,	10,	'https://fiverrnew.cybersoft.edu.vn/images/cv3.jpg',	'\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.',	'US$10\r\nSave up to 20% with Subscribe to Save\r\nBASIC CORPORATE BRAND IDENTITY Business Card + Letterhead + Compliment Design\r\n\r\n2 Days Delivery\r\nUnlimited Revisions\r\nIncludes logo design\r\nLogo usage guidelines\r\nColor palette\r\nTypography guidelines',	4,	3,	1),
(2,	'I will design minimal logo with complete corporate brand identity',	66,	10,	'https://fiverrnew.cybersoft.edu.vn/images/cv4.jpg',	'\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.',	'US$10\r\nSave up to 20% with Subscribe to Save\r\nBASIC CORPORATE BRAND IDENTITY Business Card + Letterhead + Compliment Design\r\n\r\n2 Days Delivery\r\nUnlimited Revisions\r\nIncludes logo design\r\nLogo usage guidelines\r\nColor palette\r\nTypography guidelines',	3,	3,	1),
(3,	'I will create an effective instagram hashtag growth strategy',	522,	20,	'https://fiverrnew.cybersoft.edu.vn/images/cv5.jpg',	'\nHi There,\r\n\r\n\r\n\r\nHave You Been Looking for a Brand Logo with Complete Corporate Brand Identity?\r\n\r\n\r\n\r\nHi, I am Talha, a Passionate Professional Graphic Designer designing brand logos & brand Identity for many Years, I have successfully designed brand logos with the complete corporate brand identity for different brands around the world.\r\n\r\n\r\n\r\nI also would love to do demanded designs, Just leave your request, I will respond instantly.',	'US$10\r\nSave up to 20% with Subscribe to Save\r\nBASIC CORPORATE BRAND IDENTITY Business Card + Letterhead + Compliment Design\r\n\r\n2 Days Delivery\r\nUnlimited Revisions\r\nIncludes logo design\r\nLogo usage guidelines\r\nColor palette\r\nTypography guidelines',	3,	5,	1),
(4,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv6.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	5,	1),
(5,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv7.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	5,	1),
(6,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv8.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	5,	1),
(7,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv9.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	1,	1),
(8,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv10.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	1,	1),
(9,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv11.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	2,	1),
(10,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv12.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	2,	1),
(11,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv13.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	3,	1),
(12,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv14.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	5,	1),
(13,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv14.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	5,	1),
(14,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv14.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	5,	1),
(15,	'I will design professional social media post',	55,	30,	'https://fiverrnew.cybersoft.edu.vn/images/cv14.jpg',	'\r\nWelcome to our Social Media Posts Design.\r\n\r\nWe understand the client\'s needs and assist through the whole process with professionalism.\r\n\r\nWe are experienced in creating social media graphic content for clients all over the world, for business, products, and personal brands.\r\n\r\nWe can assist you to improve the appearance of your social media presence.',	'US$30\r\nSave up to 10% with Subscribe to Save\r\nBASIC 3 unique social media posts with the most attractive content that will transform your business.\r\n\r\n2 Days Delivery\r\n3 social posts created',	2,	5,	1);

DROP TABLE IF EXISTS `LoaiCongViec`;
CREATE TABLE `LoaiCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tenLoaiCongViec` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `LoaiCongViec` (`id`, `tenLoaiCongViec`) VALUES
(1,	'Graphics & Design'),
(2,	'Digital Marketing'),
(3,	'Writing & Translation'),
(4,	'Video & Animation'),
(5,	'Music & Audio'),
(6,	'Life Style'),
(7,	'Project Management'),
(8,	'Technical');

DROP TABLE IF EXISTS `NguoiDung`;
CREATE TABLE `NguoiDung` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female','unknow') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `skill` json DEFAULT NULL,
  `certification` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `NguoiDung` (`id`, `name`, `email`, `password`, `phone`, `birthday`, `gender`, `role`, `skill`, `certification`) VALUES
(1,	'Dat Ngo',	'tom@gmail.com',	'$2a$10$5tvwY25jO69CrZQMojatiOPQG6fy.iQUIN1lP68eVq1LxbYEMcYbm',	'033746995',	'12-12-1122',	'male',	'user',	'[\"javascript, ReactJs\"]',	'[\"Cyber Academy\"]'),
(2,	'tom',	'tom1@gmail.com',	'$2a$10$c1sahmMd1EABQLHL/BJFheAPzVdhX1Uzz3eKksznueJnf1M77S1Zy',	'033746995',	'12-12-1122',	'male',	'user',	'[\"javascript, ReactJs\"]',	'[\"Cyber Academy\"]'),
(3,	'Tony Stack',	'Tony@gmail.com',	'$2a$10$TSfMXA.fkgQYbAz9wsHycuVHN/YL2qM2WGq0ypcF6zQ35R.R2GZzS',	'033746995',	'12-12-1122',	'male',	'user',	'[\"javascript, ReactJs\"]',	'[\"Cyber Academy\"]'),
(4,	'David',	'david@gmail.com',	'$2a$10$lQRL0QY8lQqDe4Gow.sEMuMWFFi6eifQ6m5/RIvqyS3tPi9f5JaFO',	'033746995',	'12-12-1122',	'male',	'user',	'[\"javascript, ReactJs\"]',	'[\"Cyber Academy\"]');

DROP TABLE IF EXISTS `NhomChiTietLoai`;
CREATE TABLE `NhomChiTietLoai` (
  `id` int NOT NULL AUTO_INCREMENT,
  `tenNhomChiTietLoaiCongViec` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maChiTietLoai` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maChiTietLoai` (`maChiTietLoai`),
  CONSTRAINT `NhomChiTietLoai_ibfk_4` FOREIGN KEY (`maChiTietLoai`) REFERENCES `LoaiCongViec` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `NhomChiTietLoai` (`id`, `tenNhomChiTietLoaiCongViec`, `maChiTietLoai`) VALUES
(1,	'Social & Marketing Videos',	4),
(2,	'Video Editing & Post-Production',	4),
(3,	'Music Production & Writing',	5),
(4,	'Beat Productions',	5),
(5,	'Graphics',	1),
(6,	'Writing',	3),
(7,	'Translation',	3),
(8,	'Marketing',	2),
(9,	'Media',	2);

DROP TABLE IF EXISTS `ThueCongViec`;
CREATE TABLE `ThueCongViec` (
  `id` int NOT NULL AUTO_INCREMENT,
  `maCongViec` int NOT NULL,
  `maNguoiThue` int NOT NULL,
  `ngayThue` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `hoanThanh` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `maCongViec` (`maCongViec`),
  KEY `maNguoiThue` (`maNguoiThue`),
  CONSTRAINT `ThueCongViec_ibfk_1` FOREIGN KEY (`maNguoiThue`) REFERENCES `NguoiDung` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ThueCongViec_ibfk_2` FOREIGN KEY (`maCongViec`) REFERENCES `CongViec` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2023-04-20 17:09:53
