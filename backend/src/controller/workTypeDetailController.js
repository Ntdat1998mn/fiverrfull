const { PrismaClient } = require("@prisma/client");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Lấy danh sách chi tiết loại công việc
const getWorkDetailList = async (req, res) => {
  try {
    let data = await prisma.ChiTietLoaiCongViec.findMany({
      include: { NhomChiTietLoai: true },
    });
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách công việc thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Tạo chi tiết loại công việc
const createWorkTypeDetail = async (req, res) => {
  try {
    let { maNhomChiTietLoaiCongViec, hinhAnh, tenChiTiet } = req.body;
    let workTypeGroup = await prisma.NhomChiTietLoai.findFirst({
      where: { id: maNhomChiTietLoaiCongViec },
    });
    if (workTypeGroup) {
      let model = { maNhomChiTietLoaiCongViec, hinhAnh, tenChiTiet };
      await prisma.ChiTietLoaiCongViec.create({ data: model });
      successCode(res, model, "Tạo chi tiết loại công việc thành công!");
    } else {
      failCode(res, "Nhóm loại công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Tìm kiếm chi tiết loại công việc (Phân trang tìm kiếm)
const workTypeDetailSearchPagination = async (req, res) => {
  try {
    let { pageIndex, pageSize, keyWork } = req.body;
    let data =
      await prisma.$queryRaw`select * from (select * from ChiTietLoaiCongViec 
        limit ${pageSize} offset ${(pageIndex - 1) * pageSize}) as Page
        where tenChiTiet like ${`%${keyWork}%`};`;
    if (data.length > 0) {
      successCode(res, data, "Lấy chi tiết loại công việc thành công!");
    } else {
      failCode(res, "Chi tiết loại công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy chi tiết loại công việc theo id
const getWorkTypeDetailById = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.ChiTietLoaiCongViec.findFirst({ where: { id } });
    if (data) {
      successCode(res, data, "Lấy chi tiết loại công việc thành công!");
    } else {
      failCode(res, "Chi tiết loại công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Sửa chi tiết loại công việc
const editWorkTypeDetail = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let { tenChiTiet, hinhAnh } = req.body;
    let model = { tenChiTiet, hinhAnh };
    await prisma.ChiTietLoaiCongViec.update({
      data: model,
      where: { id },
    });
    successCode(res, model, "Chỉnh sửa chi tiết loại công việc thành công!");
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Chi tiết loại công việc không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Xoá chi tiết loại công việc
const deleteWorkTypeDetail = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.ChiTietLoaiCongViec.delete({ where: { id } });
    successCode(res, data, "Xoá chi tiết loại công việc thành công!");
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Chi tiết loại công việc không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Thêm nhóm vào danh sách chi tiết loại công việc
const createGroupWorkTypeDetail = async (req, res) => {
  try {
    let { tenNhomChiTietLoaiCongViec, maChiTietLoai } = req.body;
    let workTypeDetail = await prisma.LoaiCongViec.findFirst({
      where: { id: maChiTietLoai },
    });
    if (workTypeDetail) {
      let model = { tenNhomChiTietLoaiCongViec, maChiTietLoai };
      await prisma.NhomChiTietLoai.create({ data: model });
      successCode(res, model, "Thêm nhóm loại công việc thành công!");
    } else {
      failCode(res, "Loại công việc không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Thêm sửa nhóm của danh sách chi tiết loại công việc
const editGroupWorkTypeDetail = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let { tenNhomChiTietLoaiCongViec } = req.body;
    let model = { tenNhomChiTietLoaiCongViec };
    await prisma.NhomChiTietLoai.update({
      data: model,
      where: { id },
    });
    successCode(
      res,
      model,
      "Chỉnh sửa nhóm chi tiết loại công việc thành công!"
    );
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Nhóm chi tiết loại công việc không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

module.exports = {
  getWorkDetailList,
  createWorkTypeDetail,
  getWorkTypeDetailById,
  editWorkTypeDetail,
  deleteWorkTypeDetail,
  createGroupWorkTypeDetail,
  editGroupWorkTypeDetail,
  workTypeDetailSearchPagination,
};
