import React from "react";
import { useSelector } from "react-redux";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive/Responsive";
import "./Market.css";

export default function Market() {
  const groupWorkList = useSelector((state) => state.jobSlice.groupJob);
  const marketItems = groupWorkList.map((groupWok, index) => {
    return (
      <div key={index}>
        <a href={`/title/${groupWok.id}`}>
          <div className="img">
            <img
              width={"48px"}
              className="main-categories-img mx-auto"
              src={groupWok.hinhAnh}
              alt="Graphics & Design"
              loading="lazy"
            />
          </div>
          <h3 className="pt-5">{groupWok.tenLoaiCongViec}</h3>
        </a>
      </div>
    );
  });
  const marketMobileItems = groupWorkList.map((groupWok, index) => {
    return (
      <div key={index}>
        <a href={`/title/${groupWok.id}`}>
          <div className="img">
            <img
              width={"48px"}
              className="main-categories-img mx-auto"
              src={groupWok.hinhAnh}
              alt="Graphics & Design"
              loading="lazy"
            />
          </div>
          <h3 className="pt-5">{groupWok.tenLoaiCongViec}</h3>
        </a>
      </div>
    );
  });
  return (
    <div id="market">
      <div className="w-4/5 mx-auto py-10">
        <h2 className="text-3xl">Explore the marketplace</h2>
        <div className="marketContent py-5">
          <Desktop>
            <div className="grid grid-cols-4 gap-5 py-5 text-center">
              {marketItems}
            </div>
          </Desktop>
          <Tablet>
            <div className="grid grid-cols-4 gap-5 py-5 px-auto text-center">
              {marketItems}
            </div>
          </Tablet>
          <Mobile>
            <div className="grid grid-cols-2 gap-5 py-5 text-center">
              {marketMobileItems}
            </div>
          </Mobile>
        </div>
      </div>
    </div>
  );
}
