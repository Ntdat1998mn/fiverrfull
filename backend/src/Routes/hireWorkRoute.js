const express = require("express");
const {
  getHirdeWorkList,
  hireWork,
  getHiredWorkById,
  editHiredWork,
  deleteHiredWork,
  getHirdeWorkListByUserId,
  hiredWorkSearchPagination,
} = require("../controller/hireWorkController");
const { verifyToken } = require("../utils/jwt");
const hireWorkRoute = express.Router();

hireWorkRoute.get("", getHirdeWorkList);
hireWorkRoute.post("", verifyToken, hireWork);
hireWorkRoute.get("/hire-work-list-by-user-id/:id", getHirdeWorkListByUserId);
hireWorkRoute.get("/hire-work-search-pgination", hiredWorkSearchPagination);
hireWorkRoute.get("/:id", getHiredWorkById);
hireWorkRoute.put("/:id", verifyToken, editHiredWork);
hireWorkRoute.delete("/:id", verifyToken, deleteHiredWork);

module.exports = hireWorkRoute;
