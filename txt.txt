services:
  frontend:
    image: mysql
    container_name: fiverr_frontend
    build: ./frontend
    ports:
      - "3000:3000"
    depends_on:
      - backend
    networks:
      - fiverr_network  
  backend:
    build: ./backend
    container_name: fiverr_backend
    ports:
      - "8080:8080"
    environment:
      - DATABASE_URL=mysql://root:1234@mysql:3306/fiverr
    depends_on:
      - mysql
    networks:
      - fiverr_network
  mysql:
    image: mysql:latest
    ports:
      - 3306:3306
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: 1234
      MYSQL_DB: fiverr
    networks:
      - fiverr_network  
    volumes:
        -mysql_db:/var/lib/mysql
  adminer:
    image: adminer
    ports:
      - "8081:8080"