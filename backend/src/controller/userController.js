const { PrismaClient } = require("@prisma/client");
const bcrypt = require("bcryptjs");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

// Lấy danh sách người dùng
const getUserList = async (req, res) => {
  try {
    let data = await prisma.NguoiDung.findMany();
    if (data.length > 0) {
      data.forEach((user) => {
        user.password = "";
      });
      successCode(res, data, "Lấy danh sách người dùng thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

//Thêm người dùng
const createUser = async (req, res) => {
  try {
    let {
      name,
      email,
      password,
      phone,
      birthday,
      role,
      gender,
      skill,
      certification,
    } = req.body;
    let model = {
      password: bcrypt.hashSync(password, 10),
      name,
      email,
      role,
      phone,
      birthday,
      gender,
      skill,
      certification,
    };
    let checkEmail = await prisma.nguoiDung.findFirst({ where: { email } });
    if (checkEmail) {
      failCode(res, "Email đã tồn tại!");
    } else {
      let data = await prisma.nguoiDung.create({ data: model });
      data.password = "";
      successCode(res, data, "Đăng ký thành công!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Xoá người dùng
const deleteUser = async (req, res) => {
  try {
    let { id } = req.body;
    let data = await prisma.NguoiDung.delete({ where: { id } });
    data.password = "";
    successCode(res, data, "Xoá người dùng thành công!");
  } catch (err) {
    if (err.message.includes("not found")) {
      const error = new Error("Người dùng không tồn tại!");
      failCode(res, error.message);
    } else {
      errorCode(res, err.message);
    }
  }
};

// Tìm kiếm người dùng (phân trang tìm kiếm)
const userSearchPagination = async (req, res) => {
  try {
    let { pageIndex, pageSize, keyWork } = req.body;
    let data = await prisma.$queryRaw`select * from (select * from NguoiDung 
        limit ${pageSize} offset ${(pageIndex - 1) * pageSize}) as Page
        where name like ${`%${keyWork}%`};`;
    if (data.length > 0) {
      data.forEach((user) => {
        user.password = "";
      });
      successCode(res, data, "Lấy danh sách người dùng thành công!");
    } else {
      failCode(res, "Người dùng không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy người dùng theo id người dùng
const getUserById = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let data = await prisma.NguoiDung.findFirst({ where: { id } });
    if (data) {
      data.password = "";
      successCode(res, data, "Lấy người dùng thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Sửa thông tin người dùng
const editUser = async (req, res) => {
  try {
    let id = req.params.id * 1;
    let { name, email, phone, birthday, role, gender, skill, certification } =
      req.body;
    let model = {
      name,
      email,
      role,
      phone,
      birthday,
      gender,
      skill,
      certification,
    };
    let user = await prisma.NguoiDung.findFirst({ where: { id } });
    let checkEmail = await prisma.NguoiDung.findFirst({
      where: {
        AND: [{ email: email }, { NOT: { id: id } }],
      },
    });

    if (user) {
      if (checkEmail) {
        failCode(res, "Email đã tồn tại!");
      } else {
        await prisma.NguoiDung.update({ where: { id }, data: model });
        successCode(res, model, "Cập nhật thông tin người dùng thành công!");
      }
    } else {
      failCode(res, "Người dùng không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Tìm người dùng theo tên
const getUserListByName = async (req, res) => {
  try {
    let name = req.params.name;
    let data = await prisma.NguoiDung.findMany({
      where: { name: { contains: `%${name}%` } },
    });
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách người dùng theo tên thành công!");
    } else {
      failCode(res, "Không có dữ liệu!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = {
  getUserList,
  createUser,
  deleteUser,
  getUserById,
  editUser,
  getUserListByName,
  userSearchPagination,
};
