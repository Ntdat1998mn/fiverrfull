const express = require("express");
const {
  getWorkList,
  createWork,
  getWorkById,
  editWork,
  deleteWork,
  getWorkTypeMenu,
  getWorkByWorkTypeId,
  getWorkByWorkTypeDetailId,
  getWorkListByName,
  workSearchPagination,
} = require("../controller/workController");
const { verifyToken } = require("../utils/jwt");

const workRoute = express.Router();

workRoute.get("", getWorkList);
workRoute.post("", verifyToken, createWork);
workRoute.get("/work-search-pagination", workSearchPagination); // Nếu đặt dưới getWorkById thì sẽ lầm work-type-menu == id
workRoute.get("/work-type-menu", getWorkTypeMenu); // Nếu đặt dưới getWorkById thì sẽ lầm work-type-menu == id
workRoute.get("/:id", getWorkById);
workRoute.put("/:id", verifyToken, editWork);
workRoute.delete("/:id", verifyToken, deleteWork);
workRoute.get("/work-by-work-type-id/:id", getWorkByWorkTypeId);
workRoute.get("/work-by-work-type-detail-id/:id", getWorkByWorkTypeDetailId);
workRoute.get("/work-list/:name", getWorkListByName);

module.exports = workRoute;
