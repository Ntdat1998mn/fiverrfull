import axios from "axios";
import { BASE_URL } from "./configURL";

export const workListService = {
  getGroupWork: (id) => {
    return axios({
      url: BASE_URL + `/work/work-by-work-type-id/${id}`,
      method: "GET",
    });
  },
  getWork: (id) => {
    return axios({
      url: BASE_URL + `/work/work-by-work-type-detail-id/${id}`,
      method: "GET",
    });
  },
  getDetailWork: (id) => {
    return axios({
      url: BASE_URL + `/work/${id}`,
      method: "GET",
    });
  },
  getCommentsByWorkId: (id) => {
    return axios({
      url: BASE_URL + `/comment/work/${id}`,
      method: "GET",
    });
  },
  postComment: (model, token) => {
    return axios({
      url: BASE_URL + `/comment`,
      method: "POST",
      data: model,
      headers: { accessToken: token },
    });
  },
  postThueCongViec: (model, token) => {
    return axios({
      url: BASE_URL + `/hire-work`,
      method: "POST",
      data: model,
      headers: { accessToken: token },
    });
  },
};
